# set scrollback history to 10000 (10k)
set -g history-limit 10000
# use send-prefix to pass C-a through to application
unbind C-b
set -g prefix C-a
# resize panes using PREFIX H, J, K, L
bind H resize-pane -L 5
bind J resize-pane -D 5
bind K resize-pane -U 5
bind L resize-pane -R 5
# don't rename windows automatically
set-option -g allow-rename off
# add powershell
#source "/usr/share/tmux/powerline.conf"
#run-shell "powerline-daemon -q"
# Support for 24-bit color
set -g default-terminal "xterm-256color"
set -ga terminal-overrides ",xterm-256color:Tc"
set-option -g default-shell /usr/bin/fish
# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -T copy-mode-vi C-h select-pane -L
bind-key -T copy-mode-vi C-j select-pane -D
bind-key -T copy-mode-vi C-k select-pane -U
bind-key -T copy-mode-vi C-l select-pane -R

#ColorScheme

# Base16 Styling Guidelines:
# base00 - Default Background
# base01 - Lighter Background (Used for status bars)
# base02 - Selection Background
# base03 - Comments, Invisibles, Line Highlighting
# base04 - Dark Foreground (Used for status bars)
# base05 - Default Foreground, Caret, Delimiters, Operators
# base06 - Light Foreground (Not often used)
# base07 - Light Background (Not often used)
# base08 - Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
# base09 - Integers, Boolean, Constants, XML Attributes, Markup Link Url
# base0A - Classes, Markup Bold, Search Text Background
# base0B - Strings, Inherited Class, Markup Code, Diff Inserted
# base0C - Support, Regular Expressions, Escape Characters, Markup Quotes
# base0D - Functions, Methods, Attribute IDs, Headings
# base0E - Keywords, Storage, Selector, Markup Italic, Diff Changed

# Use colortest script from base16-shell to get color assignments.
colour01='#FB4934'
colour02='#B8BB26'
colour03='#FABD2F'
colour04='#83A598'
colour05='#D3869B'
colour06='#8EC07C'
colour07='#D5C4A1'
colour08='#665C54'
colour09='#FB4934'
colour10='#B8BB26'
colour11='#FABD2F'
colour12='#83A598'
colour13='#D3869B'
colour14='#8EC07C'
colour15='#FBF1C7'
colour16='#FE8019'
colour17='#D65D0E'
colour18='#3C3836'
colour19='#504945'
colour20='#BDAE93'
colour21='#EBDBB2'

# default statusbar colors
set -g status-style fg="$colour20",bg="$colour18",default

# default window title colors
set-window-option -g window-status-style fg="$colour20",default

# active window title colors
set-window-option -g window-status-current-style fg="$colour07",bg=default

# pane border
set -g pane-border-style fg="$colour18"
set -g pane-active-border-style fg="$colour19"

# message text
set -g message-style fg="$colour07",bg="$colour18"

# pane number display
set -g display-panes-active-colour "$colour02" # base0B
set -g display-panes-colour "$colour03" # base0A

# clock
set-window-option -g clock-mode-colour "$colour02" #base0B

# bell
set-window-option -g window-status-bell-style fg=colour18,bg=colour01
# base01, base 08

#Status Bar Configuration

set-window-option -g status-left " #S "
set-window-option -g status-left-style fg=black,bg=white

set-window-option -g status-right " %H:%M %d-%b-%y "
set-window-option -g status-right-style fg=black,bg=white

set-window-option -g window-status-format " #I: #W "

set-window-option -g window-status-current-format " #I: #W "
set-window-option -g window-status-current-style fg=green,bg=black
