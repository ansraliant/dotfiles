#Required Linters

##Javascript
You need to install `eslint` for javacript linting. Using npm install it globally, but beware, when linting a js file it will complain it does not have many packages.
Needless to say, `eslint` is installed with `npm`.

##Git Commit
`pip install gitlint` linter is loaded automatically

##PHP Language Server
Not a parsed per se, but well.
First of all install `composer require felixfbecker/language-server`.
Then we need to parse the stubs and stuff `composer run-script --working-dir=vendor/felixfbecker/language-server parse-stubs` check for the correct vendor directory. Possibly .composer on the home or something.
Run it `php vendor/felixfbecker/language-server/bin/php-language-server.php`
Then ale should use it right away

##Sources
[Ale Vim Plugin](https://github.com/w0rp/ale)
