#!/bin/bash
echo "Removing Unnecesary Configuration Files"
rm -f "$HOME/.vimrc"

echo "Installing Vundle"
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

echo "Creating Vim Backup, Undo, Swap and Syntax Folders"
mkdir "$HOME/.vim"
mkdir "$HOME/.vim/backup"
mkdir "$HOME/.vim/swap"
mkdir "$HOME/.vim/undo"
mkdir "$HOME/.vim/syntax"

echo "Creating SimLinks"
ln -s "$HOME/git/dotfiles/vim/vimrc" "$HOME/.vimrc"

echo "Installing required packages"
sudo zypper --quiet install --no-confirm ack nodejs npm ShellCheck
echo "Installing Node Dependant linters"
sudo npm install -g eslint
echo "Installing Python Dependant linters"
sudo pip install gitlint
echo "Installing Rust linters"
sudo zypper --quiet install --no-confirm rust rustfmt
