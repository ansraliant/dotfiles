# Dotfiles

Simple repo containing configuration files for my common used programs.

# Usage

Each branch of this repository is a theme. This branch corresponds to the theme base16-gruvbox-dark. Thanks to [chriskempson](https://github.com/chriskempson/base16).

# Screenshots

## Neo Vim
![](screenshots/neovim.jpg)

## Tmux
![](screenshots/tmux.jpg)
