set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath
set showmode                                                        " always show what mode we're currently editing in
set shell=/bin/bash                                                 " set bash shell to avoid problems with fish
set showmatch                                                       " Show matching brackets
set backupdir=$HOME/.config/nvim/backup//                           " keep backup, swap, and undo in a single directory
set directory=$HOME/.config/nvim/swap//
set undodir=$HOME/.config/nvim/undo//
set expandtab                                                       " expands tabs into corresponding spaces
set copyindent                                                      " copy the indent of the previous line
set tabstop=2                                                       " sets tabs to 2
set softtabstop=2
set shiftwidth=2
set autowrite                                                       " auto save on buffer switch
set path+=**                                                        " magic to fuzzy find
filetype on                                                         " required
let mapleader=","                                                   " Map <Leader> key
" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

" Make sure you use single quotes
Plug 'ntpeters/vim-better-whitespace'                               " highlight trailing whitespaces
Plug 'scrooloose/nerdcommenter'                                     " Auto Comment
Plug 'chriskempson/base16-vim'                                      " Base16 Vim colorscheme
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'christoomey/vim-tmux-navigator'                               " Tmux navigator plugin
Plug 'tpope/vim-vinegar'                                            " Add functionality to newtr
Plug 'mhinz/vim-startify'                                           " Better start screen
Plug 'sheerun/vim-polyglot'                                         " Pack of support to many languages
Plug 'w0rp/ale'                                                     " Lint Engine
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }   " Fuzzy finder
Plug 'junegunn/fzf.vim'                                             " Fuzzy finder plugin extension
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }                  " Go plugins

" Initialize plugin system
call plug#end()

"------------NETRW-----------------
let g:netrw_liststyle = 3                                           " default style to tree view

"------------Searching-------------
set ignorecase                                                      " ignore case when searching
set hlsearch                                                        " highlight current searched word

"------------Mappings-------------

" Easy VIMRC editing
nmap <Leader>ev :tabedit $MYVIMRC<cr>
" Disable search highlighting
nmap <Leader><space> :nohlsearch<cr>

"-----------GraphQL--------------------
"Syntax support for graphQL. If I don't define this variable
"syntax highlighting for javascript fails
let g:graphql_javascript_tags = ["gql", "graphql", "Relay.QL"]

"------------Visuals-------------

set laststatus=2                                                    " always show status line
set cursorline                                                      " Highlight current line
set termguicolors                                                   " if you want to run vim in a terminal
colorscheme base16-gruvbox-dark-hard

"------------AutoCommands-------------

"in order to avoid vim creating eternally the same autocommands,
"create a group that is automatically erased and created each
"time you source

augroup autosourcing
  autocmd!
  autocmd BufNewFile,BufRead *.slim setlocal filetype=slim          " Configuration to recognize slim files
  autocmd BufNewFile,BufRead *.rest set filetype=rest               " Set filetype to rest
  autocmd FileType php setlocal omnifunc=phpactor#Complete          " Set correct autocomplete
  autocmd BufNewFile,BufReadPost *.md set filetype=markdown         " Set Markdown filetypes
  autocmd BufNewFile,BufReadPost *.blade.php set filetype=blade     " Set Blade filetypes
augroup END

"------------Airline-------------
let g:airline_powerline_fonts = 1
let g:airline_theme='base16'
" Enable integration with airline for vim go
let g:airline#extensions#ale#enabled = 1

"------------Custom Commands-------------
" XML Prettify, and replace it, so it doesn't bother when sourcing
command! FormatXML %!xmllint --format %

"-----------Split Management--------------------

set splitbelow                                                      " set default to split below for horizontal
set splitright                                                      " set default to right split for vertical splits


"-----------Vim Ale--------------------
" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
" Error and warning signs. Ale integration with go
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'

"-----------FZF Mappings----------------------
" Lunch buffer window
nmap <Leader>r :Buffers<CR>
" Show history of buffers
nmap <Leader>h :History<CR>
" Search everywhere on the proyect
nmap <Leader>F :Files<CR>
" Only search files under git version control
nmap <Leader>f :GFiles<CR>
" Show tags from the buffer files
nmap <Leader>t :BTags<CR>
" Show tags from tag files
nmap <Leader>T :Tags<CR>
" Show fuzzy search for help tags
nmap <Leader>H :Helptags!<CR>
" Open Ag to search text in whole proyect
nmap <Leader>a :Ag<space>

"-----------Vim Go Plugin---------------------
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
let g:go_auto_sameids = 1
let g:go_fmt_command = "goimports"

"-----------Custom Functions------------------

function! DoPrettyXML()
  " save the filetype so we can restore it later
  let l:origft = &ft
  set ft=
  " delete the xml header if it exists. This will
  " permit us to surround the document with fake tags
  " without creating invalid xml.
  1s/<?xml .*?>//e
  " insert fake tags around the entire document.
  " This will permit us to pretty-format excerpts of
  " XML that may contain multiple top-level elements.
  0put ='<PrettyXML>'
  $put ='</PrettyXML>'
  silent %!xmllint --format -
  " xmllint will insert an <?xml?> header. it's easy enough to delete
  " if you don't want it.
  " delete the fake tags
  2d
  $d
  " restore the 'normal' indentation, which is one extra level
  " too deep due to the extra tags we wrapped around the document.
  silent %<
  " back to home
  1
  " restore the filetype
  exe "set ft=" . l:origft
endfunction

" define the PrettyXML command
command! PrettyXML call DoPrettyXML()

